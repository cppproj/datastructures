//============================================================================
// Name        : SharedPtr.h
// Created on  : 05.04.2021
// Author      : Tokmakov Andrey
// Version     : 1.0
// Copyright   : Your copyright notice
// Description : TT LCache tests class 
//============================================================================

#include "Integer.h"
#include "SharedPtr.h"
#include <iostream>

namespace Memory::SharedPtr 
{
	template<typename T>
	class DefaultDeleter {
	public:
		DefaultDeleter() { 
			std::cout << "DefaultDeleter()" << std::endl; 
		}

		DefaultDeleter(const DefaultDeleter& deleter) { 
			std::cout << "DefaultDeleter(const DefaultDeleter& deleter)" << std::endl; 
		}

		DefaultDeleter(DefaultDeleter&& deleter) noexcept { 
			std::cout << "DefaultDeleter(DefaultDeleter&& deleter) noexcept" << std::endl; 
		}

		~DefaultDeleter() { 
			std::cout << "~DefaultDeleter()" << std::endl; 
		}

		void operator() (T* ptr) {
			std::cout << "DefaultDeleter called" << std::endl;
			delete ptr;
		}
	};


	template<typename T, typename Deleter>
	class my_shared_ptr;

	template<typename T, typename Deleter = DefaultDeleter<T>>
	struct ControlBlock {
	private:
		// Use count:
		size_t use_count = 1;

		/** Controlled data pointer: **/
		T* raw_ptr = nullptr;

		/** Deleter: **/
		Deleter _deleter;

	public:
		ControlBlock(T* ptr) : raw_ptr(ptr) {
			// std::cout << __LINE__ << ":" << __FUNCTION__ << std::endl;
		}

		ControlBlock(T* ptr, Deleter&& deleter) : raw_ptr(ptr), _deleter(std::move(deleter)) {
			// std::cout << __LINE__ << ":" << __FUNCTION__ << std::endl;
		}

		~ControlBlock() {
			if (this->raw_ptr)
				this->_deleter(raw_ptr);
		}

		inline void increment_use_count() noexcept {
			++use_count;
		}

		inline void decrement_use_count() noexcept {
			--use_count;
		}

		friend my_shared_ptr;
	};

	
	template<typename T, typename Deleter = DefaultDeleter<T>>
	class my_shared_ptr {
	private:
		ControlBlock<T, Deleter>* contr = nullptr;

	public:

		my_shared_ptr(T* ptr) {
			// std::cout << __LINE__ << ":" << __FUNCTION__ << std::endl;
			this->contr = new ControlBlock<T, Deleter>(ptr);
		}

		/*
		template<typename ... Params>
		my_shared_ptr(Params&& ... args) {
			// std::cout << __LINE__ << ":" << __FUNCTION__ << std::endl;
			this->contr = new ControlBlock<T, Deleter>(new T(std::forward< Params>(args)...));
		}
		*/

		my_shared_ptr(T* ptr, Deleter&& deleter) {
			// std::cout << __LINE__ << ":" << __FUNCTION__ << std::endl;
			this->contr = new ControlBlock<T, Deleter>(ptr, std::move(deleter));
		}

		my_shared_ptr(const my_shared_ptr<T>& obj) {
			contr = obj.contr;
			contr->increment_use_count();
			// std::cout << "[Copy constructor]. count = " << contr->use_count << std::endl;
		}

		my_shared_ptr& operator=(const my_shared_ptr& right) {
			if (&right != this) {
				// TODO: Refactor this code block.
				contr->decrement_use_count();
				if (0 == contr->use_count)
					delete contr;
				contr = right.contr;
				contr->increment_use_count();
			}
			//std::cout << "[Copy assignment operator]. count = " << contr->use_count << std::endl;
			return *this;
		}

		T* get() const noexcept {
			return this->contr->raw_ptr;
		}


		T* operator*() const noexcept {
			return this->contr->raw_ptr;
		}

		~my_shared_ptr() {
			contr->decrement_use_count();
			if (0 == contr->use_count)
				delete contr;
		}
	};


	template<typename _Ty, typename ... _Types>
	my_shared_ptr<_Ty> make_shared(_Types&& ... args) {
		return my_shared_ptr<_Ty>(new _Ty(std::forward<_Types>(args)...));
	}

}

namespace Memory::SharedPtr::Tests
{

	void Create_and_Destroy_Ptr() {
		my_shared_ptr<Integer> int1(new Integer(11));
	}


	void Make_Shared_Test() {
		auto var = make_shared<Integer>(1234);
	}

	//---------------------------------------------------------------------

	template<typename T>
	class CustomDeleter {
	public:
		CustomDeleter() {
			std::cout << "CustomDeleter()" << std::endl;
		}

		CustomDeleter(const CustomDeleter& deleter) { 
			std::cout << "CustomDeleter(const CustomDeleter& deleter)" << std::endl; 
		}

		CustomDeleter(CustomDeleter&& deleter) noexcept { 
			std::cout << "CustomDeleter(CustomDeleter&& deleter) noexcept" << std::endl;
		}

		~CustomDeleter() { 
			std::cout << "~CustomDeleter()" << std::endl; 
		}

		void operator() (T* ptr) {
			std::cout << "CustomDeleter called" << std::endl;
			delete ptr;
		}
	};

	template<typename T>
	void handle_shared_ptr(my_shared_ptr<T> ptr) {
		ptr.get()->printInfo();
	}

	void DefaultDeleter_Test() {
		my_shared_ptr<Integer> int1(new Integer(11));
	}


	void Copy_Test() {
		my_shared_ptr<Integer> a1(new Integer(111));
		my_shared_ptr<Integer> a2 = a1;

		std::cout << a1.get()->getValue() << std::endl;
		std::cout << a2.get()->getValue() << std::endl;
	}


	/*
	void CustomDeleter_Test() {
		CustomDeleter<Integer> deleter;
		my_shared_ptr<Integer, CustomDeleter<Integer>> intPtr(new Integer(123), std::move(deleter));
		// handle_shared_ptr(intPtr);
	}
	*/
}


namespace Memory {


	void Shared_Ptr_Tests()
	{
		// SharedPtr::Tests::Create_and_Destroy_Ptr();
		// SharedPtr::Tests::Create_and_Destroy();
		
		// SharedPtr::Tests::Make_Shared_Test();

		SharedPtr::Tests::Copy_Test();


		// SharedPtr::Tests::DefaultDeleter_Test();

		// SharedPtr::Tests::CustomDeleter_Test();
	}
}

