//============================================================================
// Name        : Logging.h
// Created on  : 22.05.2021
// Author      : Tokmakov Andrey
// Version     : 1.0
// Copyright   : Your copyright notice
// Description : C++ Logging lib
//============================================================================

#ifndef LOGGING_INCLUDE_GUARD__H
#define LOGGING_INCLUDE_GUARD__H

namespace Logging {
	
	void TEST_ALL();
}

#endif // !LOGGING_INCLUDE_GUARD__H
