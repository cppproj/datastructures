//============================================================================
// Name        : ObjectPools.h
// Created on  : 31.03.2021
// Author      : Tokmakov Andrey
// Version     : 1.0
// Copyright   : Your copyright notice
// Description : Object pools tests
//============================================================================

#ifndef OBJECT_POOLS_TESTS__H_
#define OBJECT_POOLS_TESTS__H_

namespace ObjectPools {
	void TEST_ALL();
};

#endif // (!OBJECT_POOLS_TESTS__H_)