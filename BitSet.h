//============================================================================
// Name        : BitSet.h
// Created on  : 17.05.2021
// Author      : Tokmakov Andrey
// Version     : 1.0
// Copyright   : Your copyright notice
// Description : BitSet (custom implementation) tests
//============================================================================

#ifndef BITSET_CUSTOM_TESTS__H_
#define BITSET_CUSTOM_TESTS__H_

#include <iostream>
#include <vector>

namespace BitSet {

	class BitSet
	{
	private:
		using storage_type = unsigned int;
		static constexpr size_t BLOCK_BITS_SIZE { sizeof(storage_type) * 8 };

	private:
		std::vector<storage_type> bits {0};

	public:

		void setBit(size_t bit, bool value) noexcept {
			const size_t index = (bit - 1) / BLOCK_BITS_SIZE;
			const size_t scoped_bit = (bit - 1) % BLOCK_BITS_SIZE;

			if (bits.size() <= index) 
				bits.resize(index + 1);

			if (value)
				bits[index] |= (1 << scoped_bit);
			else 
				bits[index] &= (~(1 << scoped_bit));
		}

		bool getBit(const size_t bit) const noexcept {
			if (BLOCK_BITS_SIZE * bits.size() < bit) 
				return false;
			
			const size_t index = (bit - 1) / BLOCK_BITS_SIZE;
			const size_t scoped_bit = (bit - 1) % BLOCK_BITS_SIZE;

			return bits[index] & (1 << scoped_bit);
		}
	};

	void TEST_ALL();
};

#endif // (!OPTIONAL_CUSTOM_TESTS__H_)