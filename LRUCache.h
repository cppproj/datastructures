//============================================================================
// Name        : LRUCache.h
// Created on  : 20.01.2021
// Author      : Tokmakov Andrey
// Version     : 1.0
// Copyright   : Your copyright notice
// Description : LRUCache tests
//============================================================================

#ifndef LRU_CACHE_TESTS__H_
#define LRU_CACHE_TESTS__H_

namespace LRUCache {
	void TEST_ALL();
};

#endif // (!LRU_CACHE_TESTS__H_)