//============================================================================
// Name        : Circular_Buffer.h
// Created on  : 17.01.2021
// Author      : Tokmakov Andrey
// Version     : 1.0
// Copyright   : Your copyright notice
// Description : Circular_Buffer tests class 
//============================================================================

#ifndef CIRCULAR_BUFFER_TESTS_INCLUDE_GUARD__H
#define CIRCULAR_BUFFER_TESTS_INCLUDE_GUARD__H

namespace CircularBuffers {
	void TEST_ALL();
}

#endif // !CIRCULAR_BUFFER_TESTS_INCLUDE_GUARD__H

