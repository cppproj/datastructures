//============================================================================
// Name        : TTLCache.h
// Created on  : 14.03.2021
// Author      : Tokmakov Andrey
// Version     : 1.0
// Copyright   : Your copyright notice
// Description : TT LCache tests class 
//============================================================================

#ifndef TTK_CACHE_INCLUDE_GUARD__H
#define TTK_CACHE_INCLUDE_GUARD__H

namespace TTLCache {
	void TEST_ALL();
}

#endif // !TTK_CACHE_INCLUDE_GUARD__H

