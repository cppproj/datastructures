//============================================================================
// Name        : SharedPtr.h
// Created on  : 05.04.2021
// Author      : Tokmakov Andrey
// Version     : 1.0
// Copyright   : Your copyright notice
// Description : TT LCache tests class 
//============================================================================

#ifndef MEMORY_SHARED_PTR_INCLUDE_GUARD__H
#define MEMORY_SHARED_PTR_INCLUDE_GUARD__H

namespace Memory {
	void Shared_Ptr_Tests();
}

#endif // !MEMORY_SHARED_PTR_INCLUDE_GUARD__H
